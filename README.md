# COVID-miner

Tools to build a consensus sequence for DNA vaccines.

# Consensus sequence script

Sequence consensus workflow to construct consensus sequence for covid19 by aligning GISAID nucleotide fasta sequences to the Wuhan reference, then determining the most frequently occurring nucleotide at each position.

Sequences are aligned against the Wuhan strain reference using NUCmer from the MUMmer package. This generates a delta encoded alignment file which is then parsed by the show-snps -T command to produce a catalog of all inserted, deleted and substituted bases in the query sequences compared to the corresponding reference. Show-snps output is converted into standard VCF format using code adapted from https://github.com/MatteoSchiavinato/Utilities

For consensus building, the workflow takes advantage of bcftools consensus to apply snp and indel variant calls to the Wuhan reference.
Consensus sequence is constructed separately for Spike and RBD, in addition to the consensus for the viral complete genome. Moreover, a consensus of minimal identity is constructed, applying N, D or I character at variant sites (snp, deletion and insertion, respectively) instead of the most frequent residue.
DNA consensus sequence is eventually translated to protein. \
Consensus sequences and relative protein conversions are saved in the results/[sample directory]/consensus/ directory.

Genetic variants are annotated using snpEff canonical transcripts in terms of functional effect and penetration in the viral population, and the annotated variant table collected in the results/[sample directory]/snp/ directory.


# Mutation frequencies script

The script will calculate mutation frequencies from a "snpsift" table.
Usage: Run the script with Rscript in an environment with R language installed (at least version 3.6.1) and "dplyr", "tidyr", "readr" libraries from the "tidyverse" package. Snpsift table must be tab separated. Results will be written in the current working directory by default.

# Example:
 
```
Rscript --vanilla frequencies_script.R table_snpsift.tsv
```

# Usage

```
docker run --rm -it -v [source directory]:/input covid19:latest /input/[fasta file]
```

Example:
```
docker run --rm -it -v $PWD/test:/test covid19:latest /test/sample.fasta
```



# Visualizations

All visualizations are generated automatically through the covid_miner_figures.R script.
Before all, the script checks that all necessary packages are installed and therefore provides for the installation of any missing packages.
After this, it starts to generate images and step by step displays when the plot has been done and where it has been saved.