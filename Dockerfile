FROM epruesse/biocondabot

RUN apt-get update \
    && apt-get install --yes libgfortran3 \
    && apt-get autoremove --yes && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN \
    conda install --override-channels --yes \
      -c main \
      -c bioconda \
      -c r \
        conda==4.8.3 \
        bowtie2 \
        samtools \
        bcftools \
        snpEff \
        snpsift \
        emboss \
        r-tidyverse

RUN conda install -c bioconda bioconductor-trackviewer --yes


RUN mkdir -p /covid-miner

ENV MAIN_FOLDER="/covid-miner"
ENV MAIN_SCRIPT="cov19_consensus_sequence.sh"

WORKDIR ${MAIN_FOLDER}

COPY ${MAIN_SCRIPT} /covid-miner/${MAIN_SCRIPT}
COPY my-mummer-2-vcf-allele-depth.py /covid-miner/my-mummer-2-vcf-allele-depth.py
COPY frequencies_script.R /covid-miner/frequencies_script.R
COPY processing_and_visual.R /covid-miner/processing_and_visual.R
COPY docker-entrypoint.sh /covid-miner/docker-entrypoint.sh
COPY ref /covid-miner/ref
COPY input /covid-miner/input
COPY ./docker-entrypoint.sh /usr/local/bin/docker-entrypoint

RUN chmod +x /covid-miner/*.sh /usr/local/bin/docker-entrypoint


ENTRYPOINT ["/usr/local/bin/docker-entrypoint"]
