#!/bin/bash
# WORKFLOW for Spike Protein consensus building

set -e


fasta=$1
echo "fasta file: $fasta"

#remove spaces in fasta header names 
zgrep ">" $fasta | if zgrep -q " "; then
    echo "found spaces in fasta headers"
    echo "$(dirname "$fasta")"
    cd $(dirname "$fasta")
    cp $(basename $fasta) copy_$(basename $fasta) && 
    gunzip -cd copy_$(basename $fasta) | sed -e '/^>/s/ /_/g' | gzip > $(basename $fasta);
    rm copy_$(basename $fasta)
fi

#remove IUPAC ambiguity codes
zgrep -v ">" $fasta | if zgrep -q -i [RYSWKMBDHVU]; then
    echo "found IUPAC ambiguity codes in fasta sequences"
    echo "$(dirname "$fasta")"
    cd $(dirname "$fasta")
    cp $(basename $fasta) copy_$(basename $fasta) && 
    gunzip -cd copy_$(basename $fasta) | sed -e '/^[^>]/s/[RYSWKMBDHVUryswkmbdhvu]/N/g' | gzip > $(basename $fasta);
    rm copy_$(basename $fasta)
fi

#remove nucmer invalid characters
zgrep -v ">" $fasta | if zgrep -q "_\|-"; then
    echo "found nucmer invalid character in fasta sequences"
    echo "$(dirname "$fasta")"
    cd $(dirname "$fasta")
    cp $(basename $fasta) copy_$(basename $fasta) && 
    gunzip -cd copy_$(basename $fasta) | sed -e '/^[^>]/s/_/n/g' -e '/^[^>]/s/-/n/g'  | gzip > $(basename $fasta);
    rm copy_$(basename $fasta)
fi



base=$(basename $fasta)
echo "running with ${base%%.*} fasta sequences"

sampleDir="/covid-miner/results/${base%%.*}"
mkdir -p $sampleDir

datetime=$(eval "date +"%d_%m_%y-%H_%M"") 
exec > >(tee -i $sampleDir/${datetime}.log)
exec 2>&1

cd $sampleDir


#-----step 1
# nucmer alignment of GISAID COVID-19 sequences to Wuhan reference

# to fix a show-snps bug, remove windows trailing lines
gunzip -c $fasta | tr -d '\15\32' > ${base%%.*}.fasta.clean

# align to the reference
nucmer /covid-miner/ref/covid-reference.fasta ${base%%.*}.fasta.clean -p ${base%%.*}

# identify variant sites by using the show-snps utility of the nucmer package
show-snps -T ${base%%.*}.delta > ${base%%.*}.snps

# convert show-snps output to standard VCF
# use piece of code from https://github.com/MatteoSchiavinato/Utilities
python /covid-miner/my-mummer-2-vcf-allele-depth.py -s ${base%%.*}.snps --input-header -g /covid-miner/ref/covid-reference.fasta --output-header > ${base%%.*}.vcf


#-----step 2
# downstream bcftools consensus creates consensus sequence by applying VCF variants to a reference fasta
# It does not output the consensus base (most frequent allele) at each position, always applying the ALT
# allele to the reference fasta, ignoring the depths of the REF and ALT allele.

# This command includes records for which the depth of the ALT allele exceeds the REF allele
bcftools filter -i 'INFO/AD[1] > INFO/AD[0]' ${base%%.*}.vcf > ${base%%.*}_pass_variants.vcf

bgzip -c ${base%%.*}_pass_variants.vcf > ${base%%.*}_pass_variants.vcf.gz
bcftools index ${base%%.*}_pass_variants.vcf.gz



consensusDir="$sampleDir/consensus"
mkdir -p $consensusDir

# complete genome consensus building 
bcftools consensus -f /covid-miner/ref/covid-reference.fasta ${base%%.*}_pass_variants.vcf.gz -o $consensusDir/${base%%.*}_consensus_complete_genome.fa
sed -i 's/>.*/>NC_045512.2 Severe acute respiratory syndrome coronavirus 2 consensus, complete genome/' $consensusDir/${base%%.*}_consensus_complete_genome.fa

# spike sequence consensus building 
samtools faidx /covid-miner/ref/covid-reference.fasta NC_045512.2:21563-25384 | sed '/^>/ s/$/ Severe acute respiratory syndrome coronavirus 2 consensus, Spike sequence/' | bcftools consensus ${base%%.*}_pass_variants.vcf.gz -o $consensusDir/${base%%.*}_consensus_spike_sequence.fa

# RBD consensus building
# RBD here as amino acids 319-541 of Spike protein sequence
samtools faidx /covid-miner/ref/covid-reference.fasta NC_045512.2:22517-23185 | sed '/^>/ s/$/ Severe acute respiratory syndrome coronavirus 2 consensus, Spike RBD/' | bcftools consensus ${base%%.*}_pass_variants.vcf.gz -o $consensusDir/${base%%.*}_consensus_RBD_sequence.fa



#-----step 2.1
# consensus in terms of minimal identity 

# include records only if exhibited by at least 1% of the isolates worldwide
n_sequences=$(zgrep -c ">" $fasta)
echo $n_sequences
filter_value=$(( $n_sequences/100 ))
echo $filter_value

bcftools filter -i '(INFO/AD[1]) >= '$filter_value'' ${base%%.*}.vcf > ${base%%.*}_minimum_frequency.vcf


# substitute all ALT allels in vcf with N
# this is necessary if we want a consensus with "holes" in correspondence of variants since consensus command of bcftools will 
# build consensus sequence by applying the alternative allele found in vcf (that we want here to be N) to the reference fasta
awk '!/^#/{gsub(/\A/,"N",$5) gsub(/\T/,"N",$5) gsub(/\C/,"N",$5) gsub(/\G/,"N",$5)} 1' OFS="\t" ${base%%.*}_minimum_frequency.vcf > ${base%%.*}_minimum_frequency_allele_substituted.vcf

# differentiate between indels and snps
awk '!/^#/{split($8,l,";"); split($5,a,","); if ((l[1] == "INDEL") && length(a[1]) > length($4)) gsub(/\N/,"I",$5); if ((l[1] == "INDEL") && length(a[1]) < length($4)) gsub(/\N/,"D",$5)} 1' OFS="\t" ${base%%.*}_minimum_frequency_allele_substituted.vcf > ${base%%.*}_minimum_frequency_bcftools_consensus_ready.vcf


bgzip -c ${base%%.*}_minimum_frequency_bcftools_consensus_ready.vcf > ${base%%.*}_minimum_frequency_bcftools_consensus_ready.vcf.gz
bcftools index ${base%%.*}_minimum_frequency_bcftools_consensus_ready.vcf.gz

# complete genome 
bcftools consensus -f /covid-miner/ref/covid-reference.fasta ${base%%.*}_minimum_frequency_bcftools_consensus_ready.vcf.gz -o $consensusDir/${base%%.*}_minimum_consensus_complete_genome.fa
sed -i 's/>.*/>NC_045512.2 Severe acute respiratory syndrome coronavirus 2 consensus minimal identity, complete genome/' $consensusDir/${base%%.*}_minimum_consensus_complete_genome.fa

# spike sequence
samtools faidx /covid-miner/ref/covid-reference.fasta NC_045512.2:21563-25384 | sed '/^>/ s/$/ Severe acute respiratory syndrome coronavirus 2 consensus minimal identity, Spike sequence/' | bcftools consensus ${base%%.*}_minimum_frequency_bcftools_consensus_ready.vcf.gz -o $consensusDir/${base%%.*}_minimum_consensus_spike_sequence.fa

# RBD
samtools faidx /covid-miner/ref/covid-reference.fasta NC_045512.2:22517-23185 | sed '/^>/ s/$/ Severe acute respiratory syndrome coronavirus 2 consensus minimal identity, Spike RBD/' | bcftools consensus ${base%%.*}_minimum_frequency_bcftools_consensus_ready.vcf.gz -o $consensusDir/${base%%.*}_minimum_consensus_RBD_sequence.fa



#------step 3
cd $consensusDir

# convert slice protein consensus from coding to protein 
transeq -sequence ${base%%.*}_consensus_spike_sequence.fa -outseq ${base%%.*}_consensus_spike_protein.fa
sed -i 's/>.*/>NC_045512.2:21563-25384 Severe acute respiratory syndrome coronavirus 2 consensus, Spike protein sequence/' ${base%%.*}_consensus_spike_protein.fa

transeq -sequence ${base%%.*}_consensus_RBD_sequence.fa -outseq ${base%%.*}_consensus_RBD_protein.fa
sed -i 's/>.*/>NC_045512.2:22517-23185 Severe acute respiratory syndrome coronavirus 2 consensus, Spike RBD protein sequence/' ${base%%.*}_consensus_RBD_protein.fa


sed -e '/^[^>]/s/D/N/g' -e '/^[^>]/s/I/N/g' ${base%%.*}_minimum_consensus_spike_sequence.fa > ${base%%.*}_minimum_consensus_spike_sequence_translation_ready.fa
transeq -sequence ${base%%.*}_minimum_consensus_spike_sequence_translation_ready.fa -outseq ${base%%.*}_minimum_consensus_spike_protein.fa
sed -i 's/>.*/>NC_045512.2:21563-25384 Severe acute respiratory syndrome coronavirus 2 consensus minimal identity, Spike protein sequence/' ${base%%.*}_minimum_consensus_spike_protein.fa
rm ${base%%.*}_minimum_consensus_spike_sequence_translation_ready.fa

sed -e '/^[^>]/s/D/N/g' -e '/^[^>]/s/I/N/g' ${base%%.*}_minimum_consensus_RBD_sequence.fa > ${base%%.*}_minimum_consensus_RBD_sequence_translation_ready.fa
transeq -sequence ${base%%.*}_minimum_consensus_RBD_sequence_translation_ready.fa -outseq ${base%%.*}_minimum_consensus_RBD_protein.fa
sed -i 's/>.*/>NC_045512.2:22517-23185 Severe acute respiratory syndrome coronavirus 2 consensus minimal identity, Spike RBD protein sequence/' ${base%%.*}_minimum_consensus_RBD_protein.fa
rm ${base%%.*}_minimum_consensus_RBD_sequence_translation_ready.fa

cd ..


#------step 4
snpsDir="$sampleDir/snps"
mkdir -p $snpsDir

# variant annotation via snpEff  
snpEff NC_045512.2 ${base%%.*}.vcf -canon > $snpsDir/${base%%.*}_snpeff.vcf

#parse annotated vcf to table
java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar extractFields -s "," -e "." $snpsDir/${base%%.*}_snpeff.vcf CHROM POS REF ALT "ANN[0].ANNOTATION" "ANN[0].GENE" "ANN[0].HGVS_C" "ANN[0].HGVS_P" AD > $snpsDir/${base%%.*}_snpsift.tsv

##Create tables for downstream analyses
tableDir="$sampleDir/table"
mkdir -p $tableDir

#Country -> Sequence table:
zgrep '>' $fasta > $tableDir/${base%%.*}_sequence_dictionary.tsv
cut -d/ -f 2,4 $tableDir/${base%%.*}_sequence_dictionary.tsv | sed s/\\/2020// | cut -d "|" -f 1,2 --output-delimiter=$'\t' > $tableDir/${base%%.*}_country_sequence.tsv

#Mutation -> Sequence table:
java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar extractFields $snpsDir/${base%%.*}_snpeff.vcf CHROM POS REF ALT > $tableDir/${base%%.*}_mutations_for_mut-seq_table.tsv

echo "SEQUENCE" > $tableDir/${base%%.*}_sequences_for_mut-seq_table.tsv
while read -r line; do
    echo $line | grep -o "EPI_\w*"  | tr "\n" "," | sed 's/,$/\n/'
done < $snpsDir/${base%%.*}_snpeff.vcf >> $tableDir/${base%%.*}_sequences_for_mut-seq_table.tsv

paste $tableDir/${base%%.*}_mutations_for_mut-seq_table.tsv $tableDir/${base%%.*}_sequences_for_mut-seq_table.tsv > $tableDir/${base%%.*}_mut-seq_table.tsv

#------step 4
plotsDir="$sampleDir/plots"
mkdir -p $plotDir

#Run R script for data processing and visualization
Rscript --vanilla /covid-miner/processing_and_visual.R ${base%%.*}

# TODO fix graphviz / trackviewer on container 
#Running visualization script for lolliplop
#Rscript --vanilla /covid-miner/visualize_mutations.R ${base%%.*}


