###########################################
####### FIGURES FOR COVID-MINER ###########
###########################################


#LOADING THE WORKING DIRECTORY AS A PARAMETER
args <- commandArgs(trailingOnly = T)

if (length(args) == 0 || length(args) >= 2){
  stop("Please give only one argument")
}


#locally debug
#args <- "gisaid_20210117"

run_name <- args

suppressPackageStartupMessages({
  library(tidyverse)
  library(RColorBrewer)
  library(trackViewer)
  library(seqinr)
  library(ComplexHeatmap)
  library(circlize)
  library(reshape2)
  library(factoextra)
  library(FactoMineR)
  library(scales)
  library(countrycode)
})


# DEFINING PATHS FOR THE NECESSARY TABLES TO BE LOADED --------------------
snpsift <- paste0("/covid-miner/results/",run_name,"/snps/",run_name,"_snpsift.tsv")
freq_path <- paste0("/covid-miner/results/",run_name,"/table/freq_results.tsv")
date_path <- paste0("/covid-miner/results/",run_name,"/table/seq_date_mut.tsv")
seq_dic <- paste0("/covid-miner/results/",run_name, "/table/", run_name, "_sequence_dictionary.tsv")
mut_seq <- paste0("/covid-miner/results/",run_name, "/table/", run_name, "_mut-seq_table.tsv")
cou_seq <- paste0("/covid-miner/results/",run_name,"/table/", run_name, "_country_sequence.tsv")
iedb_path <-paste0("/covid-miner/results/",run_name,"/table/iedb_pred_table.tsv")

out_folder <- paste0("/covid-miner/results/",run_name,"/plots/")
out_folder_tables <- paste0("/covid-miner/results/",run_name,"/table/")

folders <- c("folders","snpsift","freq_path","date_path","seq_dic","mut_seq","cou_seq","iedb_path","out_folder","out_folder_tables","run_name")


# Figure 1 B - Lollipop plot ----------------------------------------------

#defining function for generating lollipop plot
plot_covid_muts = function(mutations=NULL, genome=NULL, filename=NULL, start = NULL, end = NULL, title = NULL, width = NULL) {
  #setting options for the mutation visualization
  SNP = mutations$POS
  covid_muts = GRanges(mutations$CHROM[1], IRanges(SNP, width=1, names=mutations$Protein_change))
  covid_muts$score = mutations$Frequency*100
  covid_muts$lwd = 1
  
  #setting options for the genome visualization
  start_sites =  as.numeric(unlist(genome[4]))
  end_sites =  as.numeric(unlist(genome[5]))
  length_sites = end_sites - start_sites
  
  covid_genome = GRanges(genome$V1[1], IRanges(start_sites,  width=length_sites, names=genome$gene))
  #colori dei geni
  covid_genome$fill = brewer.pal(n=length(genome$gene),name = "Set3")
  #altezza dei rettangoli rappresentativi del gene
  covid_genome$height = rep(0.05, length(genome$gene))
  
  #range to plot
  range_to_plot = GRanges(mutations$CHROM[1],IRanges(start, end ))
  
  #generating and saving the plot
  png(filename,res = 300,width = width, height = 1500,units = "px")
  lolliplot(covid_muts, covid_genome, range_to_plot, ylab = "Frequency (%)", xaxis = c(start_sites, end_sites, 22517, 23185),
            cex=1,txtSize = 4, yaxis = c(0, 20, 40,60,80,100))
  grid.text(title, x=.5, y=.98, just="top", gp=gpar(cex=1.5, fontface="bold"))
  invisible(dev.off())
}

#defining function to reshape dataframe
analysis = function(freq) {
  muts <- suppressMessages(read.csv(snpsift, sep="\t", header=T)) %>% filter(ANN.0..HGVS_P != ".")
  df <-  suppressMessages(read.csv(freq_path, sep="\t", header=T)) %>% 
    dplyr::rename(POS=Position, REF=Reference , ALT = Alteration) %>%
    inner_join(muts, by=c("POS", "REF","ALT")) %>%
    filter(Frequency >= freq) %>%
    dplyr::rename(Protein_change=Mutation) 
  
  #filtering for sinonimous variants
  notkeep <- which(substr(df$Protein_change, start = 1, stop = 1)== substr(df$Protein_change,start = nchar(df$Protein_change),stop=nchar(df$Protein_change)))
  df <- df[-notkeep,]
  df <- df %>% filter(!duplicated(Protein_change))
  return(df)
}

#defining function for genome
genome_fun = function(df) {
  genome <- suppressMessages(read.csv("./ref/sequence.gff3", sep="\t", header=F, skip=2))
  genome_genes = genome [ genome[,3] == "gene", c(1:5,9)]
  genome_genes$gene <- unlist(lapply ( genome_genes[, 6] , FUN=function(x) { ss = strsplit(as.character(x), split="gene="); strsplit(ss[[1]][2], split=";")[[1]][1]  }))
  genome_genes <- genome_genes %>% filter(gene %in% unique(df$ANN.0..GENE))
  return(genome_genes)
}

# GENERATE FIGURES
# frequency 0.005
df <- analysis(0.005)
genome_genes <- genome_fun(df)  
plot_covid_muts(df, genome_genes, filename=paste0(out_folder,"05percent_whole.png"), start = 266, end = 29533, title = "All variations > 0.5% frequency", width = 6000)
plot_covid_muts(df, genome_genes, filename=paste0(out_folder,"05percent_SPIKE.png"), start = 21000, end = 26000, title = "All variations > 0.5% frequency SPIKE", width = 4500)
print(paste0("Lollipop plot for 0.005 frequency saved in ",out_folder))

# frequency 0.05
df <- analysis(0.05)
genome_genes <- genome_fun(df)  
plot_covid_muts(df, genome_genes, filename=paste0(out_folder,"5percent_whole.png"), start = 266, end = 29533, title = "All variations > 5% frequency", width = 4500)
plot_covid_muts(df, genome_genes, filename=paste0(out_folder,"5percent_SPIKE.png"), start = 21000, end = 26000, title = "All variations > 5% frequency SPIKE", width = 4500)
print(paste0("Lollipop plot for 0.05 frequency saved in ",out_folder))

# frequency 0.01
df <- analysis(0.01)
genome_genes <- genome_fun(df)  
plot_covid_muts(df, genome_genes, filename=paste0(out_folder, "1percent_whole.png"), start = 266, end = 29533, title = "All variations > 1% frequency", width = 6000)
plot_covid_muts(df, genome_genes, filename=paste0(out_folder,"1percent_SPIKE.png"), start = 21000, end = 26000, title = "All variations > 1% frequency SPIKE", width = 4500)
print(paste0("Lollipop plot for 0.01 frequency saved in ",out_folder))

# frequency from 0.05 to 0.1
#need to redefine plot_covid_muts for better visualization
plot_covid_muts = function(mutations=NULL, genome=NULL, filename=NULL, start = NULL, end = NULL, title = NULL) {
    #setting options for the mutation visualization
    SNP = mutations$POS
    covid_muts = GRanges(mutations$CHROM[1], IRanges(SNP, width=1, names=mutations$Protein_change))
    covid_muts$score = mutations$Frequency*100
    covid_muts$lwd = 1
    
    #setting options for the genome visualization
    start_sites =  as.numeric(unlist(genome[4]))
    end_sites =  as.numeric(unlist(genome[5]))
    length_sites = end_sites - start_sites
    
    covid_genome = GRanges(genome$V1[1], IRanges(start_sites,  width=length_sites, names=genome$gene))
    #colori dei geni
    covid_genome$fill = brewer.pal(n=length(genome$gene),name = "Set3")
    #altezza dei rettangoli rappresentativi del gene
    covid_genome$height = rep(0.05, length(genome$gene))
    
    
    #range to plot
    range_to_plot = GRanges(mutations$CHROM[1],IRanges(start, end ))
    
    #generating and saving the plot
    png(filename,res = 300,width = 4500,height = 1500,units = "px")
    lolliplot(covid_muts, covid_genome, range_to_plot, ylab = "Frequency (%)", xaxis = c(start_sites, end_sites, 22517, 23185),
              cex=1,txtSize = 4)
    grid.text(title, x=.5, y=.98, just="top", gp=gpar(cex=1.5, fontface="bold"))
    invisible(dev.off())
  }

df <- analysis(0.005)
df <- df[df$Frequency < 0.1,]
genome_genes <- genome_fun(df)  
plot_covid_muts(df, genome_genes, filename=paste0(out_folder,"05percent_10percent_SPIKE.png"), start = 21000, end = 26000, title = "Variations from 0.5% to 10% frequency SPIKE")
print(paste0("Lollipop plot for 0.005 to 0.1 frequency saved ",out_folder))

print("Figure 1B completed")

rm(list = setdiff(ls(),folders))


# Figure 1 C - Barplot RBD variations -----------------------------------
freq_results <- suppressMessages(read_delim(freq_path, "\t", escape_double = FALSE, trim_ws = TRUE)) %>% 
  filter(Position > 22517 & Position < 23185) %>%
  mutate(log2_Freq = log2(Frequency + 1))

#filtering for sinonimous variants
notkeep <- which(substr(freq_results$Mutation, start = 1, stop = 1)== substr(freq_results$Mutation, start = nchar(freq_results$Mutation),stop=nchar(freq_results$Mutation)))
freq_results <- freq_results[-notkeep,]

freq_results <- freq_results[order(freq_results$log2_Freq, decreasing = T), ]
freq_results <- freq_results[1:10,]

png(paste0(out_folder,"most_freq_RBD_var.png"),width = 8000,height = 3000,res = 300)
ggplot(freq_results, aes(x=reorder(Mutation,-log2_Freq), y=log2_Freq, fill = Frequency )) +
  geom_bar(stat = "identity") +
  theme_light()+
  ylab("log2(Freq + 1)") +
  xlab("RBD Variations") +
  ggtitle("Most Frequent RBD variations")+
  theme(title = element_text(size = 35), 
        axis.text.x=element_text(angle=45,hjust=1,size = 32,face = "bold"), 
        axis.text.y=element_text(size=32, face = "bold"),
        axis.title.y = element_text(size = 35 , vjust = 5, face = "bold"),
        axis.title.x = element_blank(),
        panel.grid.major = element_line(size = 1, linetype = 'solid',colour = "grey95"),
        panel.grid.minor = element_line(size = 1, linetype = 'solid',colour = "grey95"),
        legend.position = "right", legend.title = element_text(size = 20), 
        legend.text = element_text(size = 18), legend.key.height = unit(1, "cm"),
        plot.margin = unit(c(0,0,0,2.5), "cm")) +
  geom_text(data=freq_results, aes(label=round(Frequency*100,digits = 2)),vjust=-.5, color="black", size=8) +
  scale_fill_continuous(low="#3288BD", high="#9E0142", breaks = c(min(freq_results$Frequency), max(freq_results$Frequency)/2, max(freq_results$Frequency) ),
                        labels = c(paste0(round(min(freq_results$Frequency*100),digits = 2), "%"),
                                   paste0(round(max(freq_results$Frequency*100)/2,digits = 2), "%"),
                                   paste0(round(max(freq_results$Frequency*100),digits = 2), "%")))
invisible(dev.off())

print(paste0("Barplot (Figure 1C) completed and saved in ", out_folder))

rm(list = setdiff(ls(),folders))


# Figure 2 A - Heatmap RBD variants ---------------------------------------

cs <- suppressMessages(read_delim(cou_seq, delim = '\t', col_names = F))
cs <- filter(cs, X2!="NA")
names(cs) <- c("Country", "Seq")
cs$Country <- gsub("_","",cs$Country)
cs$Country <- gsub("/*","", cs$Country)
ms <- suppressMessages(read_delim(mut_seq, delim = '\t', col_names = T))
conv <- suppressMessages(read_delim(snpsift, col_names = T, delim = '\t'))

#adj matrix
adj_ms <- ms %>% filter(POS>22517 & POS<23185) %>% separate_rows(SEQUENCE,sep=",")
ms <- dcast(adj_ms, POS + REF + ALT ~ SEQUENCE , value.var = "SEQUENCE", fill = 0, fun.aggregate = length)
ms[,-c(1:3)] <- ifelse(ms[,-c(1:3)]!= 0, 1, 0)

dummy <- ms %>% 
  gather(.,starts_with("EPI"), key = "SEQUENCE", value = "Mutations") %>%
  left_join(.,cs,by=c("SEQUENCE"="Seq")) %>%
  group_by(., Country,POS,REF,ALT) %>%
  summarize(., mutations=sum(Mutations), .groups = 'keep')

dummy_h <- spread(dummy, key="Country", value = "mutations")

dummy_h[is.na(dummy_h)] <- 0

join <- dummy_h %>% 
  left_join(., conv, by=c("POS","ALT","REF")) %>% 
  select(-CHROM, -`ANN[0].ANNOTATION`,-`ANN[0].GENE`, -`ANN[0].HGVS_C`, -AD) %>% 
  dplyr::rename(Protein_change=`ANN[0].HGVS_P`) %>%
  ungroup(POS, REF, ALT) %>%
  select(-POS, -REF, -ALT) %>%
  mutate(Mut_EXT = Protein_change) %>%
  extract(Protein_change,into = c("a1", "p", "a2"), regex = "([aA-zZ]+)([0-9]+)([aA-zZ]+)") %>% 
  mutate(a1=suppressWarnings(a(a1)), p=p, a2=suppressWarnings(a(a2))) %>% 
  mutate(a2=ifelse(substr(Mut_EXT, start = nchar(Mut_EXT)-1,stop = nchar(Mut_EXT)) == "fs","fs",a2)) %>%
  unite(aa,a1,p,a2, sep='') %>%
  select(-Mut_EXT)

join$aa <- make.unique(join$aa, sep = '_')
join <- join %>% column_to_rownames(var = "aa") %>% select(which(colSums(.)!=0))

join <- join[rowSums(join != 0) > 0,]

country <- colnames(join)

norm_factors <- data.frame(table(cs$Country)) %>%
  filter(Var1%in%country) 

factors <- norm_factors$Freq
names(factors)<-country

norm <- join
for (cou in country){
  norm[[cou]] <- round((join[[cou]]/factors[[cou]])*10^3, digits = 2)
}

notkeep <- which(substr(rownames(norm), start = 1, stop = 1)== substr(rownames(norm),start = 5,stop=5))
norm_filt <- norm[-notkeep,]
norm_filt <- norm_filt[,which(colSums(norm_filt)!=0)]

join_filt <- join[-notkeep,]
join_filt <- join_filt[,which(colSums(join_filt)!=0)]

#write.table(x = join_filt,file = paste0(out_folder_tables,"source_table_RDB_variants.tsv"), sep = '\t', quote = F, col.names = NA, row.names = T)
#print(paste0("RBD binary matrix saved in ",out_folder_tables))

#strong Filtering for better visualization
thresh_mut <- 100
join_filt_mut <- join_filt[which(rowSums(join_filt) >= thresh_mut),]
norm_filt_mut <- norm_filt[rownames(join_filt_mut),]
join_filt_mut <- join_filt_mut[,colSums(join_filt_mut)!=0]
norm_filt_mut <- norm_filt_mut[,colnames(join_filt_mut)]

thresh_count <- 50
join_filt_count <- join_filt_mut[, colSums(join_filt_mut) >= thresh_count]
norm_filt_count <- norm_filt_mut[,colnames(join_filt_count)]
join_filt_count <- join_filt_count[,colSums(join_filt_count)!=0]
norm_filt_count <- norm_filt_count[,colnames(join_filt_count)]


col <- colorRamp2(c(0,0.1,3,4,10,50), c("#ccdeed", "#FEE08B","#FDAE61", "#F46D43","#D53E4F","#9E0142"))
png(paste0(out_folder,"Heatmap_freq_filtered.png"), width = 3500, height = 3000, units="px",res=300)
draw(Heatmap(as.matrix(norm_filt_count), col=col, 
             show_row_dend = F, show_column_dend = F,
             rect_gp = gpar(col="white", lwd=.2), row_names_gp = gpar(fontsize = 9), 
             column_names_gp = gpar(fontsize=9), name = "MpTI", 
             heatmap_legend_param = list(legend_direction="horizontal", 
                                         title_position='topcenter', 
                                         legend_width=unit(2,'cm'), 
                                         labels_gp=gpar(fontsize=7), 
                                         title_gp = gpar(fontsize = 7, fontface = "bold")), 
             cell_fun = function(j,i,x,y,width, height, fill){
               if(join_filt_count[i, j] > 0) grid.text(sprintf("%.1d", join_filt_count[i,j]),x,y,
                                                        gp=gpar(fontsize=6, col="grey12",fontface="bold.italic"))}),  
     heatmap_legend_side='bottom', column_title="RBD variants distribution (absolute count in cell)")
invisible(dev.off())

print(paste0("RBD Heatmap (Figure 2A) completed and saved in ", out_folder))

rm(list = setdiff(ls(),c(all_of(folders),c("adj_ms","cs","conv","join_filt"))))


# Figure 2 B - MCA --------------------------------------------------------
mca_data <- inner_join(adj_ms, cs , by =c("SEQUENCE"="Seq"))
conv2 <- conv %>% select(POS, `ANN[0].HGVS_P`)
mca_data <- inner_join(mca_data, conv2, by = "POS") %>%
  select(-CHROM, -REF, -ALT) %>%
  mutate(ext=`ANN[0].HGVS_P`) %>%
  extract(`ANN[0].HGVS_P`,into = c("a1", "p", "a2"), regex = "([[:alnum:]]+)([0-9][0-9][0-9])([[:alnum:]]+)") %>% 
  mutate(a1=suppressWarnings(a(a1)), p=p, a2=suppressWarnings(a(a2))) %>% 
  mutate(a2=ifelse(substr(ext, start = nchar(ext)-1,stop = nchar(ext)) == "fs","fs",a2)) %>%
  filter(!is.na(a1) & !is.na(a2)) %>% 
  unite(aa,a1,p,a2, sep='') %>%
  filter(aa %in% rownames(join_filt)) %>% #strategy to select only RBD variants
  select(-ext)

mca_data <- dcast(mca_data, SEQUENCE + Country ~ aa , value.var = "aa", fill = 0, fun.aggregate = length) %>%
  column_to_rownames(var = "SEQUENCE")


mca_data[-1] <- ifelse(mca_data[-1]!=0,"y","n") 
mca_data <- mca_data[order(mca_data$Country),]

mca_data <-  mca_data[, c("Country", "N439K", "S477N","N501Y")]

res <- MCA(mca_data, graph = F)
png(paste0(out_folder, "MCA_sel_var.png"), width = 3000,height = 1500,res = 300)
fviz_mca_var(res, repel = T, col.var = "contrib", gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),  ggtheme = theme_minimal()) +
  theme(axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        axis.text.x = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks = element_blank(),
        panel.grid = element_blank())+
  xlim(-2, 5) +
  ylim(-3, 5)
invisible(dev.off())

print(paste0("MCA plot (Figure 2B) completed and saved in ", out_folder))

rm(list = setdiff(ls(),folders))


# Building tables for Figure 2 C  -------------------------------------------
build_tables <- function(mut) {
  seq_name <- paste0("./results/",run_name,"/rbd/seq_name_",mut,".txt") 
  ss <- suppressMessages(read.csv(seq_name ,sep=",", header=F)) %>% t()
  ss2 <- str_split_fixed(ss, "/",n=Inf)
  ss3 <- str_split_fixed(ss2[,4], "\\|",n=Inf)
  final_df <- cbind.data.frame(ss2[,1:3], ss3)
  month <- str_split_fixed(final_df[,6], "-", 3) [,2]
  final_df$month <- month
  colnames(final_df) <- c("virus", "country", "name", "year", "sequence", "date","month")
  final_df$month <- gsub(":.*","",final_df$month)
  final_df <- final_df %>% filter(month != "") %>% mutate(month = as.numeric(month))
  write.table(final_df, paste0("./results/",run_name,"/rbd/final_",mut,"_distribution.tsv"), sep="\t",quote=F,row.names=F)
  print(paste0("Table for ",mut," mutation completed and saved in ./results/",run_name,"/rbd/final_",mut,"_distribution.tsv"))

}

build_tables("S477N")
build_tables("N439K")
build_tables("N501Y")

# Figure 2 C - Time distribution RBD --------------------------------------

time_function <- function(mut) {
  df_path <- paste0("./results/",run_name,"/rbd/final_",mut,"_distribution.tsv")
  df <- suppressMessages(read.table(df_path, header = T, sep = "\t")) %>%
    group_by(., country, month) %>%
    summarize(., count=sum(month), .groups = 'keep') %>%
    mutate(count = count/month)
  
  filt_sum <- aggregate(df$count, by=list(country=df$country), FUN=sum) %>%
    filter(! x < 10)
  
  countries <- filt_sum$country
  
  df <- df[df$country %in% countries, ]
  
  sd <- suppressMessages(read_delim(seq_dic, delim = '\t', col_names = F)) %>% t()
  
  #splitting the data to extract the month
  sd2 <- str_split_fixed(sd, "/",n=Inf)
  sd3 <- str_split_fixed(sd2[,4], "\\|",n=Inf)
  
  final_df <- cbind.data.frame(sd2[,1:3], sd3)
  month <- str_split_fixed(final_df[,6], "-", 3) [,2]
  final_df$month = month
  
  colnames(final_df) = c("virus", "country", "name", "year", "sequence", "date","month")
  
  final_df <- final_df %>% 
    filter(month != "")%>%
    filter(country %in% countries)
  
  #computing the total sequence per country per month
  final_df$month <- as.numeric(final_df$month)
  tb <- as.data.frame(table(final_df$month, final_df$country))
  names(tb) <- c("month", "country", "tot")
  tb$month <- as.numeric(as.character(tb$month))
  
  data <- full_join(df, tb, by=c("country", "month"))
  data[is.na(data)] <- 0
  data$freq <- log2((data$count/data$tot)*100 +1 )
  data$freq[data$freq == "NaN"] <- 0
  
  data$country <- gsub("_"," ", data$country)                  
  data$country <- countrycode(data$country, "country.name", "iso3c", custom_match = c("Scotland" = "SCO", "Wales" = "WAL", "England"="GBR", "Northern Ireland"="N_IRL"))
  
  # removing months that has no sequences
  data <- data %>%
    group_by(month) %>%
    filter(!all(count==0)) 

  #generating and saving the plot
  png(paste0(out_folder,"time_distrib_",mut,"_complete.png"), width = 1000, height = 2000, res = 300)
  pl<-  ggplot(data, aes(x  = month, y = freq, fill = count)) + 
    theme_light()+
    ggtitle(paste0("Distribution of ", mut, " mutation in viral isolates over time")) +
    ylab("log2(freq + 1)") +
    xlab("Collection Date") +
    geom_bar(stat = "identity", width = 0.3)+
    facet_grid(country ~ . , scales = "fixed", shrink = T, switch = "y") +
    theme(legend.position = "bottom" , legend.key.height = unit(0.3,"line"),legend.key.width = unit(1, "line"),
          legend.text=element_text(size=5),title = element_text(size = 7.5),
          plot.caption = element_text(hjust = 0),plot.title.position = "plot",
          axis.text.x=element_text(angle=45,hjust=1,size = 8), 
          axis.text.y=element_text(size=6), axis.title.x = element_text(vjust = -3),
          axis.title.y = element_text(size = 8),
          panel.grid.major = element_line(size = 0.5, linetype = 'solid',colour = "grey95"),
          panel.grid.minor = element_line(size = 0.5, linetype = 'solid',colour = "white")) +
    scale_x_continuous(breaks = seq(1, 12, by = 1), labels = c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug", "Sep", "Oct", "Nov", "Dec")) +
    scale_fill_continuous(low="#3288BD", high="#9E0142", breaks = c(min(data$count), max(data$count)/2,max(data$count))) +
    geom_text(data=subset(data, count > 0), aes(label=count),vjust=-.5, color="black", size=2) +
    theme(strip.background = element_rect(fill="antiquewhite3")) +
    ylim(0,10)
  
  print(pl)
  invisible(dev.off())
}

time_function("S477N")
print(paste0("Time distribution for S477N mutation completed and saved in ",out_folder))
time_function("N439K")
print(paste0("Time distribution for N439K mutation completed and saved in ",out_folder))
time_function("N501Y")
print(paste0("Time distribution for N501Y mutation completed and saved in ",out_folder))

rm(list = setdiff(ls(),folders))

# Figure 3 A - IEDB heatmap -----------------------------------------------
#iedb <- suppressMessages(read_delim(iedb_path, delim='\t', col_names = T))
#hla_a0101 <- as.data.frame(iedb %>% select(HGVSP, WT, Mutant_cons, contains("HLA-A0101_score")))
#hla_a0201 <- as.data.frame(iedb %>% select(HGVSP, WT, Mutant_cons, contains("HLA-A0201_score")))
#hla_a0301 <- as.data.frame(iedb %>% select(HGVSP, WT, Mutant_cons, contains("HLA-A0301_score")))
#hla_a2402 <- as.data.frame(iedb %>% select(HGVSP, WT, Mutant_cons, contains("HLA-A2402_score")))
#hla_b0702 <- as.data.frame(iedb %>% select(HGVSP, WT, Mutant_cons, contains("HLA-B0702_score")))

#RdBu <- c("#CA0020", "#F4A582", "#F7F7F7", "#92C5DE", "#0571B0")

#rownames(hla_b0702) <- paste0("row", 1:length(rownames(hla_b0702)))
#rowlabels <- structure(hla_b0702[,1], names=paste0("row", 1:length(rownames(hla_b0702))))

#ha = rowAnnotation(Peptide = anno_text(hla_a0101[,3],location = 0.5, just="center",width = unit(1.5,'cm') ,  gp = gpar(col = "black", border = "black", fontsize=5)))

#ht_a0101 <- Heatmap(as.matrix(hla_a0101[,4:5]), col = colorRamp2(c(-0.4,-0.2,0,0.2,0.4), RdBu) ,  name='IEDB score', show_column_dend = F, rect_gp = gpar(col="black", lwd=.2), row_names_gp = gpar(fontsize = 7), column_names_gp = gpar(fontsize=10), column_labels = gsub("HLA-A0101_score_", "", colnames(hla_a0101[,4:5]), fixed=T), column_order = order(colnames(hla_a0101[,4:5]), decreasing = T), column_title = "HLA-A0101" , column_title_gp = gpar(fontsize=8),heatmap_legend_param = list(legend_direction="horizontal", title_position='topcenter', legend_width=unit(3,'cm'), labels_gp=gpar(fontsize=7), title_gp = gpar(fontsize = 7, fontface = "bold")) )
#ht_a0201 <- Heatmap(as.matrix(hla_a0201[,4:5]),col = colorRamp2(c(-0.4,-0.2,0,0.2,0.4), RdBu) , show_heatmap_legend = F, show_column_dend = F, rect_gp = gpar(col="black", lwd=.2), row_names_gp = gpar(fontsize = 7), column_names_gp = gpar(fontsize=10), column_labels = gsub("HLA-A0201_score_", "", colnames(hla_a0201[,4:5]), fixed=T), column_order = order(colnames(hla_a0201[,4:5]), decreasing = T), column_title = "HLA-A0201", column_title_gp = gpar(fontsize=8) )
#ht_a0301 <- Heatmap(as.matrix(hla_a0301[,4:5]),col = colorRamp2(c(-0.4,-0.2,0,0.2,0.4), RdBu) , show_heatmap_legend = F, show_column_dend = F, rect_gp = gpar(col="black", lwd=.2), row_names_gp = gpar(fontsize = 7), column_names_gp = gpar(fontsize=10), column_labels = gsub("HLA-A0301_score_", "", colnames(hla_a0301[,4:5]), fixed=T), column_order = order(colnames(hla_a0301[,4:5]), decreasing = T), column_title = "HLA-A0301", column_title_gp = gpar(fontsize=8))
#ht_a2402 <- Heatmap(as.matrix(hla_a2402[,4:5]),col = colorRamp2(c(-0.4,-0.2,0,0.2,0.4), RdBu) , show_heatmap_legend = F, show_column_dend = F, rect_gp = gpar(col="black", lwd=.2), row_names_gp = gpar(fontsize = 7), column_names_gp = gpar(fontsize=10), column_labels = gsub("HLA-A2402_score_", "", colnames(hla_a2402[,4:5]), fixed=T), column_order = order(colnames(hla_a2402[,4:5]), decreasing = T), column_title = "HLA-A2402", column_title_gp = gpar(fontsize=8))
#ht_b0702 <- Heatmap(as.matrix(hla_b0702[,4:5]),col = colorRamp2(c(-0.4,-0.2,0,0.2,0.4), RdBu) , right_annotation = ha, row_labels = rowlabels[rownames(hla_b0702)],  show_heatmap_legend = F , show_column_dend = F, rect_gp = gpar(col="black", lwd=.2), row_names_gp = gpar(fontsize = 7), column_names_gp = gpar(fontsize=10), column_labels = gsub("HLA-B0702_score_", "", colnames(hla_b0702[,4:5]), fixed=T), column_order = order(colnames(hla_b0702[,4:5]), decreasing = T), column_title = "HLA-B0702", column_title_gp = gpar(fontsize=8), heatmap_legend_param = list(legend_direction="horizontal", title_position='topcenter', legend_width=unit(3,'cm'), labels_gp=gpar(fontsize=7), title_gp = gpar(fontsize = 7, fontface = "bold")) )

#ht_list <- ht_a0101+ht_a0201+ht_a0301+ht_a2402+ht_b0702

#png(paste0(out_folder,"Heatmap_IEDB.png"), width = 2000, height = 1500, units="px",res=300)
#draw(ht_list, heatmap_legend_side='bottom', column_title="IEDB immunogenicity scores")
#invisible(dev.off())

#print(paste0("IEDB Heatmap completed and saved in ",out_folder))

#rm(list = setdiff(ls(),folders))

# Preparing table for Figure 3C (and other analysis) ---------------------

sd <- suppressMessages(read_delim(seq_dic, delim = '\t', col_names = F)) %>% t()
sd2 <- str_split_fixed(sd, "/",n=Inf)
sd3 <- str_split_fixed(sd2[,4], "\\|",n=Inf)
final_df <- cbind.data.frame(sd2[,1:3], sd3)
month <- str_split_fixed(final_df[,6], "-", 3) [,2]
final_df$month <- month
colnames(final_df) <- c("virus", "country", "name", "year", "sequence", "date","month")
final_df <- final_df %>% filter(month != "") %>% select(country, sequence, date)

final_df$date[grep("^[[:digit:]]+-[[:digit:]]+$",final_df$date)] <- paste0(final_df$date[grep("^[[:digit:]]+-[[:digit:]]+$",final_df$date)], "-01")
final_df$date <- as.Date(final_df$date)

ms <- suppressMessages(read_delim(mut_seq, delim = '\t', col_names = T) %>% separate_rows(SEQUENCE,sep=",")) %>% select(-CHROM)

conv <- suppressMessages(read_delim(snpsift, col_names = T, delim = '\t')) %>% 
  filter(`ANN[0].HGVS_P`!=".") %>%
  mutate(`ANN[0].HGVS_P`=gsub("Ter","Stp",`ANN[0].HGVS_P`)) %>% 
  mutate(Mut_EXT=`ANN[0].HGVS_P`) %>%
  extract(`ANN[0].HGVS_P`,into = c("a1", "p", "a2"), regex = "([aA-zZ]+)([0-9]+)([aA-zZ]+)") %>%
  filter(!is.na(a1) & !is.na(a2)) %>%
  mutate(a1=suppressWarnings(a(a1)), p=p, a2=suppressWarnings(a(a2))) %>% 
  mutate(a2=ifelse(substr(Mut_EXT, start = nchar(Mut_EXT)-1,stop = nchar(Mut_EXT)) == "fs","fs",a2)) %>%
  filter(!is.na(a1) & !is.na(a2)) %>%
  unite(aa,a1,p,a2, sep='') 

notkeep <- which(substr(conv$aa, start = 1, stop = 1)== substr(conv$aa,start = nchar(conv$aa),stop=nchar(conv$aa)))
conv <- conv[-notkeep,]

join1 <- inner_join(final_df,ms, by=c("sequence"="SEQUENCE"))

join2 <- inner_join(join1, conv, by=c("POS","REF","ALT")) %>%
  select(-`ANN[0].GENE`, -`ANN[0].HGVS_C`, -AD, -CHROM, -`ANN[0].ANNOTATION`) %>%
  dplyr::rename(Mutation=aa)

write.table(join2, paste0(out_folder_tables,"seq_date_mut.tsv"), quote = F, sep = "\t", row.names = F, col.names = T)

print(paste0("Table containing association between date, mutation and sequence saved in ",out_folder_tables))

rm(list = setdiff(ls(),folders))


# Figure 3 C - Donuts plot ------------------------------------------------

seq_date_mut <- suppressMessages(read_delim(date_path, "\t", escape_double = FALSE, trim_ws = TRUE))
num_seq <- length(unique(seq_date_mut$sequence))
rbd_only <- seq_date_mut %>% filter(POS > 22517 & POS < 23185)
num_seq_mut_RBD <- length(unique(rbd_only$sequence))
perc <- round((num_seq_mut_RBD/num_seq)*100, digits=2)
print(paste0(num_seq," sequences with date information, ",num_seq_mut_RBD," of wich have mutations in RBD, that corresponds to ",perc," %."))

seq_date_mut$date <- as.Date(seq_date_mut$date, format= "%Y-%m-%d")
from_sept <- seq_date_mut %>% subset(date> "2020-09-01")
num_seq_sept <- length(unique(from_sept$sequence))
rbd_only_sept <- from_sept %>% filter(POS > 22517 & POS < 23185)
num_seq_mut_RBD_sept <- length(unique(rbd_only_sept$sequence))
perc_sept <- round((num_seq_mut_RBD_sept/num_seq_sept)*100, digits=2)
print(paste0("From 2 September, ",num_seq," sequences with date information, ",num_seq_mut_RBD," of wich have mutations in RBD, that corresponds to ",perc," %."))


#donut plot for all WT/MUT RBD 
to_plot <- data.frame(type = c("MUT","WT"), counts=c(num_seq_mut_RBD,num_seq-num_seq_mut_RBD), 
                      percentage=c((num_seq_mut_RBD/num_seq)*100,((num_seq-num_seq_mut_RBD)/num_seq)*100))
to_plot$labelPosition <- c(((100+to_plot$percentage)/2)[2], ((100+to_plot$percentage)/2)[1])
to_plot$label <- paste0(to_plot$type, "\n", round(to_plot$percentage,digits = 2), "%")

colors_donut <- c("#D73027", "#4575B4")

png(paste0(out_folder,"donut_all.png"),width = 2000,height = 2000,res = 300)
ggplot(data = to_plot, aes(x = 2, y = percentage, fill = type)) + 
  geom_col(color = "black") +
  coord_polar("y", start = 1) +
  scale_fill_manual(values=colors_donut) +
  theme(panel.background = element_blank(),
        axis.line = element_blank(),
        axis.text = element_blank(),
        axis.ticks = element_blank(),
        axis.title = element_blank(),
        plot.title = element_text(hjust = 0, size = 20, vjust = -8),
        legend.position = "none") + 
  geom_text( x=2, aes(y=labelPosition, label=label, colour = type, fontface="bold"), size=5, ) +
  scale_color_manual(values=c(MUT="white",WT="white")) +
  annotate("text", x = 0, y = 0, label = paste0("Total sequences: ", num_seq, "\nAltered in RBD: ", num_seq_mut_RBD), size = 5) +
  ggtitle("Amount of WT/MUT RBD sequences") +
  xlim(0, 2.5 )
invisible(dev.off())

print(paste0("Overall donut (Figure 3C) completed and saved in ", out_folder))

#donut plot from september WT/MUT RBD
to_plot <- data.frame(type = c("MUT","WT"), counts=c(num_seq_mut_RBD_sept,num_seq_sept-num_seq_mut_RBD_sept), 
                      percentage=c((num_seq_mut_RBD_sept/num_seq_sept)*100,((num_seq_sept-num_seq_mut_RBD_sept)/num_seq_sept)*100))
to_plot$labelPosition <- c(((100+to_plot$percentage)/2)[2], ((100+to_plot$percentage)/2)[1])
to_plot$label <- paste0(to_plot$type, "\n", round(to_plot$percentage,digits = 2), "%")

colors_donut <- c("#D73027", "#4575B4")

png(paste0(out_folder,"donut_from_sept.png"),width = 2000,height = 2000,res = 300)
ggplot(data = to_plot, aes(x = 2, y = percentage, fill = type)) + 
  geom_col(color = "black") +
  coord_polar("y", start = 1) +
  scale_fill_manual(values=colors_donut) +
  theme(panel.background = element_blank(),
        axis.line = element_blank(),
        axis.text = element_blank(),
        axis.ticks = element_blank(),
        axis.title = element_blank(),
        plot.title = element_text(hjust = 0, size = 20, vjust = -8),
        legend.position = "none") + 
  geom_text( x=2, aes(y=labelPosition, label=label, colour = type, fontface='bold'), size=5) +
  annotate("text", x = 0, y = 0, label = paste0("Total sequences: ", num_seq_sept, "\nAltered in RBD: ", num_seq_mut_RBD_sept), size = 5) +
  scale_color_manual(values=c(MUT="white",WT="white")) +
  ggtitle("Amount of WT/MUT RBD sequences - from September") +
  xlim(0, 2.5 )  
invisible(dev.off())

print(paste0("From September donut (Figure 3C) completed and saved in ", out_folder))

rm(list = ls())

print("All figures for covid-miner done!")
