FUNZIONAMENTO:
(mi metto nella cartella /covid-miner) 
1. docker build -t covid_figures -f ./plots-docker/Dockerfile .  (impiega circa 1 ora e 45 minuti)
2. docker run --rm -v /mnt/c/Users/Eleonora/Desktop/covid-miner/:/covid-miner covid_figures
(impiega circa 25 minuti)

Issues:

	- runnare lo script tables_Figure_2C.sh 

			tentativo: 	RUN chmod +x ./covid-miner/plots-docker/tables_Figure_2C.sh
						RUN ./covid-miner/plots-docker/tables_Figure_2C.sh
			errore: 
						RUN ./covid-miner/plots-docker/tables_Figure_2C.sh:
						#13 0.634 /bin/sh: 1: ./covid-miner/plots-docker/tables_Figure_2C.sh: not found
						------
						executor failed running [/bin/sh -c ./covid-miner/plots-docker/tables_Figure_2C.sh]: exit code: 127
				-> generata immagine con Dockerfile "semplificato" :
						FROM rocker/r-base:4.0.3
						## create folder
						RUN mkdir -p /covid-miner
						## copy covid-miner
						COPY / /covid-miner
					accedendo con docker run --rm -it covid_immagine /bin/bash e navigando dentro, 


	- nel Dockerfile creo le cartelle plots (e rbd), quando lancio docker run --rm -v /mnt/c/Users/Eleonora/Desktop/covid-miner/:/covid-miner covid_immagine, se queste cartelle non sono già presenti in locale ottengo errore (da R):  No such file or directory Execution halted, ma se le genero va tutto bene. Come fare in modo che si generino anche in locale in modo automatico? 

	- nel Dockerfile, parametrizzare la variabile "gisaid_20210117" per lanciare lo script
		va fatto con "ARG" o con "ENV", ma non so bene come
		CMD Rscript --vanilla /covid-miner/plots-docker/covid_miner_figures_docker.R gisaid_20210117



TODO:
	- migliorare visualizzazione MCA
	- migliorare visualizzazioni distribuzione temporale (2020/2021)