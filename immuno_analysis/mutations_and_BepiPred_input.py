import os
import sys
import csv

from Bio import SeqIO
from Bio.Data.IUPACData import protein_letters_3to1
from Bio.SeqRecord import SeqRecord

mutation_table = os.path.join(sys.argv[1], "%s_immuno_analysis_snpsift_better.tsv") % (sys.argv[2])
print(mutation_table)
file_handler = open(mutation_table, "r")
reader = csv.DictReader(file_handler, delimiter='\t')

record_ref = SeqIO.read("/covid-miner/ref/reference_spike_protein.fa", "fasta")
record_cons = SeqIO.read(os.path.join(sys.argv[1], "%s_immuno_analysis_spike_protein.fa") % (sys.argv[2]), "fasta") 
record_mut = SeqIO.read(os.path.join(sys.argv[1], "%s_immuno_analysis_spike_protein_applied_variants.fa") % (sys.argv[2]), "fasta")

with open(os.path.join(sys.argv[1], "Bepipred_input_spike.fasta"), "w") as output_handle_spike, open(os.path.join(sys.argv[1], "Bepipred_input_RBD.fasta"), "w") as output_handle_RBD :
    d = dict()
    RBD_mutations = []
    for row in reader:
        pos = row['POS']
        hgvsp = row['ANN[0].HGVS_P']
        pos = []
        amino_acids = []
        for e in hgvsp[2:]:
            if e.isnumeric():
                pos.append(e)
        pp = int(''.join(pos))

        if record_cons.seq[pp - 1] == 'X':
            for l in hgvsp[2:]:
                if l.isalpha():
                    amino_acids.append(l)
            R = ''.join(amino_acids)[:3]
            A = ''.join(amino_acids)[3:]
            # if A == '':
            # print("stop codon")
            # continue
            if record_ref.seq[pp - 1] == protein_letters_3to1[R]:
                print("REF allele:", R)
                if record_mut.seq[pp - 1] == protein_letters_3to1[A]:
                    print("ALT allele:", A)
                    print("mutation is verified")
                    mutation = protein_letters_3to1[R] + str(pp) + protein_letters_3to1[A]

                    spike_mutant = record_ref.seq[:pp - 1] + record_mut.seq[pp - 1] + record_ref.seq[pp:]
                    output_spike = SeqRecord(spike_mutant, id="NC_045512.2:21563-25384 " + mutation,
                                             description="Severe acute respiratory syndrome coronavirus 2 mutant Spike protein")
                    #print(output_spike.format("fasta"))
                    SeqIO.write(output_spike, output_handle_spike, "fasta")

                    if (pp >= 319) == True and (pp <= 541) == True:
                        RBD_mutations.append(hgvsp)
                        RBD_mutant = record_ref.seq[318:pp - 1] + record_mut.seq[
                            pp - 1] + record_ref.seq[
                                      pp:541]
                        output_RBD = SeqRecord(RBD_mutant, id="NC_045512.2:22517-23185 " + mutation,
                                               description="Severe acute respiratory syndrome coronavirus 2 mutant RBD protein")
                        SeqIO.write(output_RBD, output_handle_RBD, "fasta")

                        window_cons = record_cons.seq[pp - 9:pp + 8]
                        window_ref = record_ref.seq[pp - 9:pp + 8]
                        window_mut = record_mut.seq[pp - 9:pp + 8]

                        window = record_ref.seq[pp - 9:pp - 1] + record_cons.seq[pp - 1] + record_ref.seq[
                                                                                           pp:pp + 8]

                        start = 0
                        end = 9
                        for a in window[0:(len(window)) - 8]:
                            peptide = window[start:end]
                            # print(peptide)
                            start = start + 1
                            end = end + 1

                            key = peptide
                            d.setdefault(key, [])
                            if hgvsp[2:] not in d[key]:
                                d[key].append(mutation)

with open(os.path.join(sys.argv[1], "mutation-peptide.tsv"), "w") as output:
    writer = csv.DictWriter(output, delimiter='\t', fieldnames=['Mutant'] + ['HGVSP'])
    writer.writeheader()

    #print(d.items())
    for k, v in d.items():
        mut = ''.join(v)
        print(mut)
        print(k)

        writer.writerow({'Mutant': k, 'HGVSP': mut})

    print(RBD_mutations)
