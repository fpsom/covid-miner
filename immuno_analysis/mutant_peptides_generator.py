import os
import sys
import csv

wt_file = os.path.join(sys.argv[1], "9mer-ref.fa")
mut_cons_file = os.path.join(sys.argv[1], "9mer-cons.fa")
mut_applied_file = os.path.join(sys.argv[1], "9mer-cons-applied-variants.fa")

from Bio import SeqIO
import re

wt_dict = dict()
mut_cons_dict = dict()
mut_applied_dict = dict()
blacklist = list()
append_wt = dict()
append_cons = dict()
append_mut = dict()
append = dict()
for wt in SeqIO.parse(wt_file, "fasta"):
    wt_dict[wt.id] = wt.seq
for mut_cons in SeqIO.parse(mut_cons_file, "fasta"):
    mut_cons_dict[mut_cons.id] = mut_cons.seq
for mut_applied in SeqIO.parse(mut_applied_file, "fasta"):
    mut_applied_dict[mut_applied.id] = mut_applied.seq
for id, mut_cons_seq in mut_cons_dict.items():
    counter = mut_cons_seq.count('X')
    if counter > 1:
        #print(counter)
        wt_seq = wt_dict[id]
        mut_applied_seq = mut_applied_dict[id]
        blacklist.append(str(id))
        pattern = re.compile(r'X')
        # add a sequence for each X occurrence
        for m in re.finditer(pattern, str(mut_cons_seq)):
            position = m.start()
            reduced_string = wt_seq
            reduced_string = reduced_string[:position] + mut_cons_seq[position] + reduced_string[position + 1:]
            print(reduced_string)
            append_cons[id + "." + str(position)] = reduced_string

            reduced_string2 = wt_seq
            reduced_string2 = reduced_string2[:position] + mut_applied_seq[position] + reduced_string2[position + 1:]
            print(reduced_string2)
            append_mut[id + "." + str(position)] = reduced_string2
            append_wt[id + "." + str(position)] = wt_seq

mut_cons_dict.update(append_cons)
mut_applied_dict.update(append_mut)
wt_dict.update(append_wt)

mut_cons_dict = {k: v for k, v in mut_cons_dict.items() if k not in blacklist}
mut_applied_dict = {k: v for k, v in mut_applied_dict.items() if k not in blacklist}
wt_dict = {k: v for k, v in wt_dict.items() if k not in blacklist}

for k, v in mut_cons_dict.items():
    append.setdefault(k, [])
    append[k].append(wt_dict[k])
    append[k].append(mut_cons_dict[k])
    append[k].append(mut_applied_dict[k])
#print(append)

with open(os.path.join(sys.argv[1], "mutant_peptides.tsv"), "w") as output:
    writer = csv.DictWriter(output, delimiter='\t',
                            fieldnames=['sliding_window'] + ['WT'] + ['Mutant_cons'] + ['Mutant'])
    writer.writeheader()

    no_duplicate = []
    with open(os.path.join(sys.argv[2], "netmhc_input_cons.txt"), mode="w") as f1, open(os.path.join(sys.argv[2], "netmhc_input_mut.txt"), mode="w") as f2, open(
            os.path.join(sys.argv[2], "netmhc_input_wt.txt"), mode="w") as f3:
        for k, v in append.items():
            netmhc_input_cons = str()
            netmhc_input_mut = str()
            netmhc_input_wt = str()
            if 'X' in v[1]:
                netmhc_input_cons = netmhc_input_cons + str(v[1]) + '\n'
                netmhc_input_mut = netmhc_input_mut + str(v[2]) + '\n'
                if v[0] not in no_duplicate:
                    netmhc_input_wt = netmhc_input_wt + str(v[0]) + '\n'
                    no_duplicate.append(v[0])
                    print(k)
                    print(v[0])
                    print(v[1])
                    print(v[2])

                    writer.writerow({'sliding_window': k, 'WT': v[0], 'Mutant_cons': v[1], 'Mutant': v[2]})
            f1.write(netmhc_input_cons)
            f2.write(netmhc_input_mut)
            f3.write(netmhc_input_wt)