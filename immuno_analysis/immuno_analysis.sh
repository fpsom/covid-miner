#!/bin/bash

fasta=$1
echo "fasta file: $fasta"

base=$(basename $fasta)
echo "running with ${base%%.*} fasta sequences"


sampleDir="/covid-miner/results/${base%%.*}"
mkdir -p $sampleDir

datetime=$(eval "date +"%d_%m_%y-%H_%M"") 
exec > >(tee -i $sampleDir/${datetime}.log)
exec 2>&1

immunoDir="$sampleDir/immuno"
mkdir -p $immunoDir

cd $immunoDir


#-----include records only if exhibited by at least 0.1% of the isolates worldwide

n_sequences=$(zgrep -c ">" $fasta)
echo $n_sequences

#93930
filter_value=$( bc <<< $n_sequences*0.1/100 )
echo $filter_value

bcftools filter -i '(INFO/AD[1]) >= '$filter_value'' ../${base%%.*}.vcf > ${base%%.*}_minimum_frequency_immuno_analysis.vcf



#-----variants having frequency >= 0.1% are annotated 

snpEff NC_045512.2 ${base%%.*}_minimum_frequency_immuno_analysis.vcf -canon > ${base%%.*}_immuno_analysis_snpeff.vcf

java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar filter "(ANN[0].EFFECT has 'missense_variant') && (ANN[0].GENE = 'S')" ${base%%.*}_immuno_analysis_snpeff.vcf > ${base%%.*}_immuno_analysis_snpeff_filtered.vcf

# parse annotated vcf to table
java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar extractFields -e "." ${base%%.*}_immuno_analysis_snpeff_filtered.vcf CHROM POS REF ALT "ANN[0].ANNOTATION" "ANN[0].GENE" "ANN[0].HGVS_C" "ANN[0].HGVS_P" AD > ${base%%.*}_immuno_analysis_snpsift.tsv




#-----spike and RBD "consensus building" with N characters in corrispondence of variants with 0.1% frequency in viral isolates population 

awk '!/^#/{gsub(/\A/,"N",$5) gsub(/\T/,"N",$5) gsub(/\C/,"N",$5) gsub(/\G/,"N",$5)} 1' OFS="\t" ${base%%.*}_immuno_analysis_snpeff_filtered.vcf > ${base%%.*}_minimum_frequency_immuno_analysis_undetermined_allele.vcf

bgzip -c ${base%%.*}_minimum_frequency_immuno_analysis_undetermined_allele.vcf > ${base%%.*}_minimum_frequency_immuno_analysis_undetermined_allele.vcf.gz
bcftools index ${base%%.*}_minimum_frequency_immuno_analysis_undetermined_allele.vcf.gz


# spike sequence consensus building
samtools faidx /covid-miner/ref/covid-reference.fasta NC_045512.2:21563-25384 | sed '/^>/ s/$/ Severe acute respiratory syndrome coronavirus 2 minimum frequency 0.1%, Spike sequence/' | bcftools consensus ${base%%.*}_minimum_frequency_immuno_analysis_undetermined_allele.vcf.gz -o ${base%%.*}_immuno_analysis_spike_sequence.fa

# RBD consensus building
samtools faidx /covid-miner/ref/covid-reference.fasta NC_045512.2:22517-23185 | sed '/^>/ s/$/ Severe acute respiratory syndrome coronavirus 2 minimum frequency 0.1%, Spike RBD/' | bcftools consensus ${base%%.*}_minimum_frequency_immuno_analysis_undetermined_allele.vcf.gz -o ${base%%.*}_immuno_analysis_RBD_sequence.fa

# conversion of spike from coding to protein 
transeq -sequence ${base%%.*}_immuno_analysis_spike_sequence.fa -outseq ${base%%.*}_immuno_analysis_spike_protein.fa
sed -i 's/>.*/>NC_045512.2:21563-25384 Severe acute respiratory syndrome coronavirus 2 minimum frequency 0.1%, Spike protein sequence/' ${base%%.*}_immuno_analysis_spike_protein.fa

# conversion of RBD from coding to protein
transeq -sequence ${base%%.*}_immuno_analysis_RBD_sequence.fa -outseq ${base%%.*}_immuno_analysis_RBD_protein.fa
sed -i 's/>.*/>NC_045512.2:22517-23185 Severe acute respiratory syndrome coronavirus 2 minimum frequency 0.1%, Spike RBD protein sequence/' ${base%%.*}_immuno_analysis_RBD_protein.fa






#-----spike and RBD "consensus building", this time by applying the ALT alleles for those variants having at least 0.1% frequency

bgzip -c ${base%%.*}_immuno_analysis_snpeff_filtered.vcf > ${base%%.*}_minimum_frequency_immuno_analysis_applied_allele.vcf.gz
bcftools index ${base%%.*}_minimum_frequency_immuno_analysis_applied_allele.vcf.gz

# spike
samtools faidx /covid-miner/ref/covid-reference.fasta NC_045512.2:21563-25384 | sed '/^>/ s/$/ Severe acute respiratory syndrome coronavirus 2 minimum frequency 0.1% and ALT alleles applied, Spike sequence/' | bcftools consensus ${base%%.*}_minimum_frequency_immuno_analysis_applied_allele.vcf.gz -o ${base%%.*}_immuno_analysis_spike_sequence_applied_variants.fa

# RBD
samtools faidx /covid-miner/ref/covid-reference.fasta NC_045512.2:22517-23185 | sed '/^>/ s/$/ Severe acute respiratory syndrome coronavirus 2 minimum frequency 0.1% and ALT alleles applied, Spike RBD/' | bcftools consensus ${base%%.*}_minimum_frequency_immuno_analysis_applied_allele.vcf.gz -o ${base%%.*}_immuno_analysis_RBD_sequence_applied_variants.fa

# conversion from coding to protein
transeq -sequence ${base%%.*}_immuno_analysis_spike_sequence_applied_variants.fa -outseq ${base%%.*}_immuno_analysis_spike_protein_applied_variants.fa
sed -i 's/>.*/>NC_045512.2:21563-25384 Severe acute respiratory syndrome coronavirus 2 minimum frequency 0.1% and ALT alleles applied, Spike protein sequence/' ${base%%.*}_immuno_analysis_spike_protein_applied_variants.fa

# conversion from coding to protein
transeq -sequence ${base%%.*}_immuno_analysis_RBD_sequence_applied_variants.fa -outseq ${base%%.*}_immuno_analysis_RBD_protein_applied_variants.fa
sed -i 's/>.*/>NC_045512.2:22517-23185 Severe acute respiratory syndrome coronavirus 2 minimum frequency 0.1% and ALT alleles applied, Spike RBD protein sequence/' ${base%%.*}_immuno_analysis_RBD_protein_applied_variants.fa



seqkit sliding -s 1 -W 9 ${base%%.*}_immuno_analysis_RBD_protein.fa > 9mer-cons.fa

seqkit sliding -s 1 -W 9 ${base%%.*}_immuno_analysis_RBD_protein_applied_variants.fa > 9mer-cons-applied-variants.fa

seqkit sliding -s 1 -W 9 /covid-miner/ref/reference_RBD_protein.fa > 9mer-ref.fa




mkdir -p "$immunoDir/netMHC_IEDB/input"

python /immuno_analysis/mutant_peptides_generator.py $immunoDir "$immunoDir/netMHC_IEDB/input"



#java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar filter "(ANN[0].EFFECT has 'missense_variant') && (ANN[0].GENE = 'S')" ${base%%.*}_immuno_analysis_snpeff.vcf > ${base%%.*}_immuno_analysis_snpeff_filtered.vcf

java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar filter "(ANN[0].ALLELE = ALT[0])" ${base%%.*}_immuno_analysis_snpeff_filtered.vcf > out1.vcf

java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar extractFields -e "." out1.vcf CHROM POS REF ALT "ANN[0].ANNOTATION" "ANN[0].GENE" "ANN[0].HGVS_C" "ANN[0].HGVS_P" AD > out1.tsv

java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar filter "(ANN[1].ALLELE = ALT[0]) && (ANN[1].GENE = 'S')" ${base%%.*}_immuno_analysis_snpeff_filtered.vcf > out2.vcf

java -jar /opt/conda/share/snpsift-4.3.1t-2/SnpSift.jar extractFields -e "." out2.vcf CHROM POS REF ALT "ANN[1].ANNOTATION" "ANN[1].GENE" "ANN[1].HGVS_C" "ANN[1].HGVS_P" AD > out2.tsv

sort -num -k2,2 out1.tsv out2.tsv > ${base%%.*}_immuno_analysis_snpsift_better.tsv

python /immuno_analysis/mutations_and_BepiPred_input.py $immunoDir ${base%%.*}


